#![no_std]
#![no_main]

#[macro_use(reset_fn)]
extern crate mkl27z;
use mkl27z::*;

fn main() -> ! {
  mkl27z::init();
  let p = gpio::OutputPin {
    port: gpio::PORTB_PCR18.into(),
    pddr: gpio::GPIOB_PDDR.into(),
    psor: gpio::GPIOB_PSOR.into(),
    pcor: gpio::GPIOB_PCOR.into(),
    num: 18,
  };
  p.init();
  p.high();
  let p2 = gpio::OutputPin {
    port: gpio::PORTB_PCR19.into(),
    pddr: gpio::GPIOB_PDDR.into(),
    psor: gpio::GPIOB_PSOR.into(),
    pcor: gpio::GPIOB_PCOR.into(),
    num: 19,
  };
  p2.init();
  p2.high();
  loop {}
}
reset_fn!(main);
